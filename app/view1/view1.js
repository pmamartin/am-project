'use strict';

angular.module('myApp.view1', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view1', {
            templateUrl: 'view1/view1.html',
            controller: 'View1Ctrl'
        });
    }])


    .controller('View1Ctrl', ['$scope', function ($scope) {
        $scope.seccion = "Hoteles";
        $scope.hotels = {
            "hotels": [
                {
                    "name": "Hotel Emperador",
                    "stars": "3",
                    "price": "1596"
                },
                {
                    "name": "Petit Palace San Bernardo",
                    "stars": "4",
                    "price": "2145"
                },
                {
                    "name": "Hotel Nuevo Boston",
                    "stars": "2",
                    "price": "861"
                }
            ]
        };
    }])

    .directive('numStars', [function () {

        return {
            restrict: 'E',
            scope: {
                starsInfo: '=starsNum'
            },
            templateUrl: 'view1/stars.html'
        };
    }]);